package controller;

import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.util.ArrayList;

import model.Contacts;
import model.ContactsDAO;


@WebServlet(name = "Read", urlPatterns = {"/read"})
public class Read extends javax.servlet.http.HttpServlet {

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        PrintWriter out = response.getWriter();
        StringBuilder txtContacts = new StringBuilder();
        ArrayList<Contacts> contacts;

        if (request.getParameter("search") != null) {
            contacts = ContactsDAO.read(request.getParameter("search"));
        } else {
            contacts = ContactsDAO.read();
        }

        for (Contacts item : contacts) {
            txtContacts.append(item.toString()).append("|");
        }

        if (txtContacts.length() > 0) {
            txtContacts.deleteCharAt(txtContacts.length()-1);
        }

        response.setContentType("text/html");
        response.setStatus(200);


        out.append(txtContacts.toString()).close();

        ContactsDAO.contacts.clear();

    }

}
