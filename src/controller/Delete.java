package controller;

import model.Contacts;
import model.ContactsDAO;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet(name = "Delete", urlPatterns = {"/delete"})
public class Delete extends javax.servlet.http.HttpServlet {

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        Contacts contact = new Contacts(request.getParameter("name"), request.getParameter("phone"), request.getParameter("email"));

        if (ContactsDAO.delete(contact)) {
            response.setStatus(204);
        }

        ContactsDAO.contacts.clear();


    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
