package controller;

import model.Contacts;
import model.ContactsDAO;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet(name = "Update", urlPatterns = {"/update"})
public class Update extends javax.servlet.http.HttpServlet {

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        Contacts oldContact = new Contacts(request.getParameter("name"), request.getParameter("phone"), request.getParameter("email"));
        Contacts newContact = new Contacts(request.getParameter("newName"), request.getParameter("newPhone"), request.getParameter("newEmail"));

        if (ContactsDAO.update(oldContact, newContact)) {
            response.setStatus(200);
        }

        ContactsDAO.contacts.clear();


    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

}
