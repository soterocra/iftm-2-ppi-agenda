package model;

public class Contacts {
    String name;
    String phone;
    String email;

    public Contacts(String name, String phone, String email) {
        setName(name);
        setEmail(email);
        setPhone(phone);
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String telefone) {
        this.phone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString() {
        return String.format("%s;%s;%s", getName(), getPhone(), getEmail());
    }
}
