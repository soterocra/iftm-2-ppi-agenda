package model;

import util.FileUtils;
import java.util.ArrayList;

public class ContactsDAO {

    private static String delimitator = ";";
    public static ArrayList<Contacts> contacts = new ArrayList<>();

    private static FileUtils getConnection() {

        String fullPathOfFile = "C:\\Users\\soter\\Desktop\\Arquivo Contacts\\jogadores.txt";
        FileUtils con = new FileUtils(fullPathOfFile);

        return con;
    }

    public static ArrayList<Contacts> read() {
        FileUtils con = getConnection();

        for (ArrayList<String> item : con.readFile(delimitator)) {
            contacts.add(new Contacts(item.get(0), item.get(1), item.get(2)));
        }

        return contacts;
    }

    public static ArrayList<Contacts> read(String c) {
        ArrayList<Contacts> resultOfSearch = new ArrayList<>();
        for (Contacts item : read()) {
            if (item.toString().toUpperCase().contains(c.toUpperCase())) {
                resultOfSearch.add(item);
            }
        }
        return resultOfSearch;
    }

    public static Boolean create(ArrayList<Contacts> c, Boolean append) {


        try {
            int counter = 0;
            for (Contacts item : c) {
                if (!append && counter > 0) {
                    append = Boolean.TRUE;
                }

                getConnection().writeFile(item.toString(), append);
                counter++;
            }

            return Boolean.TRUE;

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return Boolean.FALSE;
        }

    }

    public static Boolean create(Contacts c) {
        read().add(c);
        return create(contacts, Boolean.FALSE);
    }

    public static Boolean update(Contacts oldC, Contacts newC) {

        for (Contacts item : read()) {
            if (item.toString().compareTo(oldC.toString()) == 0) {
                item.setName(newC.getName());
                item.setPhone(newC.getPhone());
                item.setEmail(newC.getEmail());
            }
        }

        return create(contacts, Boolean.FALSE);
    }

    public static Boolean delete(Contacts c) {
        for (Contacts item : read()) {
            if (item.toString().compareTo(c.toString()) == 0) {
                contacts.remove(item);
                return create(contacts, Boolean.FALSE);
            }
        }
        return Boolean.FALSE;
    }

}
