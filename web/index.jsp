<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<head>
    <title>Agenda</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/restom.css">

    <script src="js/functions.js"></script>

</head>

<body onload="new PageControls().read()">

<div id="modal-create-contact" class="modal">

    <div class="modal-content">
        <span class="close">&times;</span>
        <h3 id="titleModal">Cadastrar novo contato</h3>
        <div class="linha centralized">

            <div class="col-dg-10 col-dm-10 col-dp-10">


                <div class="linha textAlignLeft">
                    <label for="newName">Nome:</label>
                    <p>
                        <input id="newName" type="text" placeholder="Nome completo do contato" style="width: 90%">
                    </p>
                </div>
                <div class="linha textAlignLeft">
                    <label for="newPhone" style="width: 20%;">Telefone: ex: (34)99648-8888</label>
                    <p>
                        <input id="newPhone" type="text" placeholder="Telefone com DDD Ex: 11, 34"
                               style="width: 90%">
                    </p>
                </div>
                <div class="linha textAlignLeft">
                    <label for="newEmail" style="width: 20%;">E-mail:</label>
                    <p>
                        <input id="newEmail" type="text" placeholder="E-mail válido" style="width: 90%">
                    </p>
                </div>

                <input id="oldStringObject" type="hidden">


            </div>
            <div class="col-dg-2 col-dm-2 col-dp-2">
                <img class="checked" src="img/icons/checked.png" alt="" onclick="new PageControls().create(document.getElementById('newName').value, document.getElementById('newPhone').value, document.getElementById('newEmail').value)">
            </div>


        </div>
    </div>

</div>

<div class="linha">
    <div class="linha centralized title">
        <h1 class="col-dg-6 col-dm-6 centralized">
            <img src="img/icons/phone.png" style="width:60px; margin-right: 20px" alt="">
            Agenda Telefônica
        </h1>
    </div>
    <div class="linha centralized">
        <div class="col-dg-6 col-dm-6 centralized" style="border-bottom: 2px solid white; margin: 20px">
            <label for="txtSearch">Buscar</label> <input id="txtSearch" style="width: 100%" type="text" name="" id="txtSearch" placeholder="Digite qualquer valor para pesquisar.">
            <img class="search" src="img/icons/search.png" alt="Buscar" title="Buscar"
                 onclick="new PageControls().read(document.getElementById('txtSearch').value)">
            <img id="btnCreateContact" class="search" src="img/icons/create.png" alt="Inserir" title="Inserir">
        </div>
    </div>


    <div id="contacts">
    <!-- Início da exibição de um usuário. -->
        <div class="contact linha centralized" style="display: none;">
            <div class="col-dg-6 col-dm-6" style="border-bottom: 2px dashed white;">
                <div class="col-dg-2 col-dm-2">
                    <img class="people" src="img/icons/people.png" alt="">
                </div>
                <div class="col-dg-8 col-dm-8 col-dp-8 textAlignLeft">
                    <div class="linha">
                        <span class="highlight">Nome: </span> <span id="name">Rafael Sotero Rocha</span>
                    </div>
                    <div class="linha">
                        <span class="highlight">Telefone: </span> <span id="phone">(34) 99648-0888</span>
                    </div>
                    <div class="linha">
                        <span class="highlight">E-mail: </span> <span id="email">soterocra@gmail.com</span>
                    </div>
                </div>
                <div class="col-dg-1 col-dm-1 col-dp-2 centralized" style="height: 95px;">
                    <img id="updateElement" class="minorIcons" src="img/icons/list.png" alt="Editar" title="Editar">
                </div>
                <div class="col-dg-1 col-dm-1 col-dp-2 centralized" style="height: 95px;">
                    <img id="removeElement" class="minorIcons" src="img/icons/remove.png" alt="Remover" title="Remover">
                </div>
            </div>
        </div>
        <!-- Fim da exibição de um usuário. -->
    </div>

</div>

<footer>


</footer>

</body>
<script src="js/modal.js"></script>

</html>