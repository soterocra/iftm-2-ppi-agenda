function PageControls() {

    var contacts = new Array();
    var modal = document.getElementById('modal-create-contact');


    this.create = function (name, phone, email) {
        var validate = verifyContact(name, phone, email);
        var stringObject = {name: name, phone: phone, email: email};
        if (validate.status) {
            if (document.getElementById("oldStringObject").value === "") {
                ajax(new PageControls().read, "/create", stringObject, "POST", 201);
            } else {
                new PageControls().update(stringObject);
            }
        } else {
            alert("Corrija o campo " + validate.key + " para prosseguir.");
        }
    };

    this.read = function (searchValue) {
        var searchObject = {};

        if (searchValue !== undefined && typeof searchValue === "string") {
            searchObject = {search: searchValue};
        }

        ajax(new PageControls().refreshContacts, "/read", searchObject, "GET", 200);
    };

    this.modalUpdate = function (oldStringObject) {
        // oldStringObject = JSON.parse(oldStringObject);
        document.getElementById("titleModal").innerHTML = "Editar Contato";
        document.getElementById("newName").value = JSON.parse(oldStringObject).name;
        document.getElementById("newPhone").value = JSON.parse(oldStringObject).phone;
        document.getElementById("newEmail").value = JSON.parse(oldStringObject).email;
        modal.style.display = "block";
        document.getElementById("oldStringObject").value = oldStringObject;
    };

    this.update = function (stringObject) {
        var oldStringObject = JSON.parse(document.getElementById("oldStringObject").value);
        // stringObject = JSON.parse(stringObject);

        var confirmResult = confirm("Tem certeza que deseja ALTERAR esse contato?");

        var mergedObjects = {
            name: oldStringObject.name,
            phone: oldStringObject.phone,
            email: oldStringObject.email,
            newName: stringObject.name,
            newPhone: stringObject.phone,
            newEmail: stringObject.email
        };

        console.log(mergedObjects);

        if (confirmResult) {
            ajax(new PageControls().read, "/update", mergedObjects, "POST", 200);
        }

    };

    this.delete = function (stringObject) {
        var confirmResult = confirm("Tem certeza que deseja EXCLUIR esse contato?");

        if (confirmResult) {
            ajax(new PageControls().read, "/delete", JSON.parse(stringObject), "POST", 204);
        }
    };

    this.refreshContacts = function (xmlDoc) {
        var contactCards = document.getElementsByClassName("contact");

        while (contactCards[contactCards.length - 1].style.display !== "none") {
            contactCards[contactCards.length - 1].outerHTML = "";
        }
        console.log(xmlDoc.responseText.split("|"));

        contacts = xmlDoc.responseText.split("|").map(xrt => xrt.split(";"));

        console.log(contacts);

        if (contacts[0][0] === "") return alert("Nada Encontrado!");

        for (var i = 0; i < contacts.length; i++) {
            var contact = {
                name: contacts[i][0],
                phone: contacts[i][1],
                email: contacts[i][2]
            };

            document.getElementById("name").innerHTML = contact.name;
            document.getElementById("phone").innerHTML = contact.phone;
            document.getElementById("email").innerHTML = contact.email;
            document.getElementById("updateElement").setAttribute("onClick", `new PageControls().modalUpdate('${JSON.stringify(contact)}')`);
            document.getElementById("removeElement").setAttribute("onClick", `new PageControls().delete('${JSON.stringify(contact)}')`);

            var clone = document.getElementsByClassName("contact")[0].cloneNode(true);
            clone.style.display = "flex";
            document.getElementById("contacts").appendChild(clone);
        }

        modal.style.display = "none";

    };

    function verifyContact(name, phone, email) {
        var regexPhone = /^\(\d{2}\)\d{5}-\d{4}$/;
        var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (name.trim() === "") {
            return {key: "Nome", status: false}
        } else {
            if (regexPhone.test(phone)) {
                if (regexEmail.test(email)) {
                    return {status: true}
                } else {
                    return {key: "E-mail", status: false}
                }
            } else {
                return {key: "Telefone", status: false}
            }
        }
    }

    function ajax(callback, relativePath, payload, requestType, codeResponse) {
        var xmlDoc = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

        payload = Object.keys(payload).map(e => (`${e}=${payload[e]}`)).join("&");

        if (requestType === "GET") {
            relativePath = relativePath + "?" + payload;
        }

        xmlDoc.open(requestType, relativePath, true);
        xmlDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xmlDoc.onreadystatechange = function () {
            if (xmlDoc.readyState === 4 && (xmlDoc.status === codeResponse)) {
                callback(xmlDoc);
            }
        };

        xmlDoc.send(payload);
    }
}


